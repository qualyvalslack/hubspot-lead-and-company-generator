const axios = require("axios");
require("dotenv").config();
const fs = require("fs");
const path = require("path");
const { App } = require("@slack/bolt");
const mongoose = require("mongoose");
const hubspot = require("@hubspot/api-client");
const slackSigningSecret = process.env.SLACK_BOT_SEC;
const slackToken = process.env.SLACK_BOT_TOKEN;
const socketToken = process.env.SLACK_SOCKET_TOKEN;
const hubspotKey = process.env.Hub_SPOT_API;
/**Importing all the diffrerent dependencies**/
//////////////////////DB connection and importing of schema////////////////////////////////
var mongoDB = `${process.env.DB}`;
mongoose.connect(mongoDB, { useNewUrlParser: true });
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.on("error", console.error.bind(console, "MongoDB connection error:"));
var contact = require("./contact");
var company = require("./company");
/** Database Connection and importing of schema **/

////initialisation of slack bot and Hubsopt crm
const app = new App({
  signingSecret: slackSigningSecret,
  token: slackToken,
  appToken: socketToken,
  socketMode: true,
  port: process.env.PORT || 8080,
});
const hubspotClient = new hubspot.Client({ apiKey: hubspotKey });

//Listnig to any and all kind of greetings
app.message(async ({ message, say }) => {
  try {
    console.log("Contact");
    if (contact.find({ _id: `${message.user}` }, { limit: 1 })) {
      console.log("Contact exists");
    } else {
      const newContact = new contact({
        _id: `${message.user}`,
      });
      newContact.save().then(() => {
        console.log("new contact created and stored in db");
      });
    }
    console.log("Company");
    if (company.find({ _id: `${message.user}` }, { limit: 1 })) {
      console.log("Company exists");
    } else {
      const newCompany = new company({
        _id: `${message.user}`,
      });
      newCompany.save().then(() => {
        console.log("new company created and stored in db");
      });
    }
  } catch (err) {
    console.log("Error due to invalid scope or invalid credentials");
  }
});
//listning to app_home_opened event
app.event("app_home_opened", async ({ event, say }) => {
  console.log(event);
});

//listning to all messages and sending appropriate reply
app.message(async ({ message, say }) => {
  try {
    console.log(message);
    await say({
      text: "Test",
      blocks: [
        {
          type: "actions",
          elements: [
            {
              type: "button",
              text: {
                type: "plain_text",
                text: "Create Company",
                emoji: true,
              },
              style: "primary",
              value: "click_me_123",
              action_id: "Create Company",
            },
          ],
        },
        {
          type: "actions",
          elements: [
            {
              type: "button",
              text: {
                type: "plain_text",
                text: "Create Contact",
                emoji: true,
              },
              style: "danger",
              value: "click_me_123",
              action_id: "Create Contact",
            },
          ],
        },
      ],
    });
  } catch (err) {
    console.log(err);
  }
});
/** Code to create new company **/
app.action("Create Company", async ({ body, client, action, ack, say }) => {
  await client.views.open({
    trigger_id: body.trigger_id,
    view: {
      type: "modal",
      callback_id: "CompanyInfo",
      title: {
        type: "plain_text",
        text: "Contact Details",
        emoji: true,
      },
      submit: {
        type: "plain_text",
        text: "Add Company",
        emoji: true,
      },
      close: {
        type: "plain_text",
        text: "Cancel",
        emoji: true,
      },
      blocks: [
        {
          type: "input",
          element: {
            type: "plain_text_input",
            action_id: "values_is",
          },
          label: {
            type: "plain_text",
            text: "Company Name",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "plain_text_input",
            action_id: "values_is",
          },
          label: {
            type: "plain_text",
            text: "Industry",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "plain_text_input",
            action_id: "values_is",
          },
          label: {
            type: "plain_text",
            text: "City",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "plain_text_input",
            action_id: "values_is",
          },
          label: {
            type: "plain_text",
            text: "State",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "plain_text_input",
            action_id: "values_is",
          },
          label: {
            type: "plain_text",
            text: "Phone Number",
            emoji: true,
          },
        },
        {
          type: "input",
          element: {
            type: "plain_text_input",
            action_id: "values_is",
          },
          label: {
            type: "plain_text",
            text: "Compay Domain",
            emoji: true,
          },
        },
      ],
    },
  });
  //console.log(action);
});

//updating of Modal, collection of data and api calls after pressing of button wirh action id CompanyInfo
app.view("CompanyInfo", async ({ ack, body, view, client, logger }) => {
  try {
    console.log(view.state.values);
    await ack({
      response_action: "update",
      view: {
        type: "modal",
        title: {
          type: "plain_text",
          text: "My App",
          emoji: true,
        },
        close: {
          type: "plain_text",
          text: "Cancel",
          emoji: true,
        },
        blocks: [
          {
            type: "section",
            text: {
              type: "plain_text",
              text: "Thank you for submitting the data.\nCompany Added",
              emoji: true,
            },
          },
        ],
      },
    });
    var keys = Object.keys(view.state.values);
    //console.log(keys);
    for (let i = 0; i < keys.length; i++) {
      //console.log(payload.state.values[keys[i]].values_is.value);
    }
    const companyName = view.state.values[keys[0]]["values_is"].value;
    const companyIndustry = view.state.values[keys[1]]["values_is"].value;
    const compnyCity = view.state.values[keys[2]]["values_is"].value;
    const companyState = view.state.values[keys[3]]["values_is"].value;
    const companyNumber = view.state.values[keys[4]]["values_is"].value;
    const companyDomain = view.state.values[keys[5]]["values_is"].value;
    const properties = {
      city: `${compnyCity}`,
      domain: `${companyDomain}`,
      industry: `${companyIndustry}`,
      name: `${companyName}`,
      phone: `${companyNumber}`,
      state: `${companyState}`,
    };
    const SimplePublicObjectInput = { properties };
    try {
      const apiResponse = await hubspotClient.crm.companies.basicApi.create(
        SimplePublicObjectInput
      );
      console.log(apiResponse);
    } catch (e) {
      e.message === "HTTP request failed"
        ? console.error(JSON.stringify(e.response, null, 2))
        : console.error(e);
    }
  } catch (err) {
    console.log(err);
  }
});
///////////////////////////////ends //////////////////////////////////////////////////////

/** Code to create contact **/

app.action("Create Contact", async ({ body, client, action, ack, say }) => {
  try {
    // console.log(action);
    await client.views.open({
      trigger_id: body.trigger_id,
      view: {
        type: "modal",
        callback_id: "ContactInfo",
        title: {
          type: "plain_text",
          text: "Contact Details",
          emoji: true,
        },
        submit: {
          type: "plain_text",
          text: "Generate Lead",
          emoji: true,
        },
        close: {
          type: "plain_text",
          text: "Cancel",
          emoji: true,
        },
        blocks: [
          {
            type: "input",
            element: {
              type: "plain_text_input",
              action_id: "values_is",
            },
            label: {
              type: "plain_text",
              text: "First Name",
              emoji: true,
            },
          },
          {
            type: "input",
            element: {
              type: "plain_text_input",
              action_id: "values_is",
            },
            label: {
              type: "plain_text",
              text: "Last Name",
              emoji: true,
            },
          },
          {
            type: "input",
            element: {
              type: "plain_text_input",
              action_id: "values_is",
            },
            label: {
              type: "plain_text",
              text: "Email address",
              emoji: true,
            },
          },
          {
            type: "input",
            element: {
              type: "plain_text_input",
              action_id: "values_is",
            },
            label: {
              type: "plain_text",
              text: "Phone Number",
              emoji: true,
            },
          },
          {
            type: "input",
            element: {
              type: "plain_text_input",
              action_id: "values_is",
            },
            label: {
              type: "plain_text",
              text: "Company",
              emoji: true,
            },
          },
          {
            type: "input",
            element: {
              type: "plain_text_input",
              action_id: "values_is",
            },
            label: {
              type: "plain_text",
              text: "Compay website",
              emoji: true,
            },
          },
        ],
      },
    });
  } catch (err) {
    console.log(err);
  }
});
//updating of Modal, collection of data and api calls after pressing of button wirh action id CompanyInfo
app.view("ContactInfo", async ({ ack, body, view, client, logger }) => {
  try {
    var keys = Object.keys(view.state.values);
    //console.log(keys);
    for (let i = 0; i < keys.length; i++) {
      console.log(view.state.values[keys[i]].values_is.value);
    }
    await ack({
      response_action: "update",
      view: {
        type: "modal",
        title: {
          type: "plain_text",
          text: "My App",
          emoji: true,
        },
        close: {
          type: "plain_text",
          text: "Cancel",
          emoji: true,
        },
        blocks: [
          {
            type: "section",
            text: {
              type: "plain_text",
              text: "Thank you for submitting the data.\nLead Generated",
              emoji: true,
            },
          },
        ],
      },
    }); //console.log(view.state.values);
    const fName = view.state.values[keys[0]]["values_is"].value;
    const lName = view.state.values[keys[1]]["values_is"].value;
    const email = view.state.values[keys[2]]["values_is"].value;
    const phoneNumber = view.state.values[keys[3]]["values_is"].value;
    const company = view.state.values[keys[4]]["values_is"].value;
    const companyWebsite = view.state.values[keys[5]]["values_is"].value;
    const properties = {
      company: `${company}`,
      email: `${email}`,
      firstname: `${fName}`,
      lastname: `${lName}`,
      phone: `${phoneNumber}`,
      website: `${companyWebsite}`,
    };
    const SimplePublicObjectInput = { properties };

    try {
      const apiResponse = await hubspotClient.crm.contacts.basicApi.create(
        SimplePublicObjectInput
      );
      console.log(apiResponse);
    } catch (e) {
      e.message === "HTTP request failed"
        ? console.error(JSON.stringify(e.response, null, 2))
        : console.error(e);
    }
  } catch (err) {
    console.log(err);
  }
});
////////////////////////////////ends//////////////////////////////////////////////
//bot is started
console.log("Port 8080 occupied");
(async () => {
  // Start the app
  await app.start(process.env.PORT || 8080);

  console.log("⚡️ Bolt app is running!");
})();
