var mongoose = require("mongoose");
const contactSchema = new mongoose.Schema({
  _id: String,
  user_fname: String,
  user_lname: String,
  user_email: String,
  user_phone: String,
  company_website: String,
  company_name: String,
});

module.exports = mongoose.model("ContactInfo", contactSchema);
