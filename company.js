var mongoose = require("mongoose");
const companySchema = new mongoose.Schema({
  _id: String,
  Company_name: String,
  Company_industry: String,
  Company_city: String,
  Company_state: String,
  Company_phone: String,
  Company_domain: String,
});

module.exports = mongoose.model("CompanyInfo", companySchema);
